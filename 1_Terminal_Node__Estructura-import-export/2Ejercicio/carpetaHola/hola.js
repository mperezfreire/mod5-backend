
const SOY_UNA_CONSTANTE = 5;

function soyUnaFuncion(){
  console.log('Hola Mundo');
}

function soyUnaFuncionConParametros(persona){
  console.log(`Hola ${persona}`);
} 

module.exports = {
  soyUnaFuncion,
  SOY_UNA_CONSTANTE,
  soyUnaFuncionConParametros,
};