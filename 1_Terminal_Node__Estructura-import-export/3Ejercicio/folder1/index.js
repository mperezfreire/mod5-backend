const { functionFolder1 } = require('./file1');
const file1 = require('./file1');
const function2 = require('./file2');
const function3 = require('./file3');

const function1a = file1.functionFolder1;


module.exports = {
  functionFolder1,
  function1a,
  function2,
  function3,
};
