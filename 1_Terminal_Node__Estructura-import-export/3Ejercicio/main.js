// import modules
const unknown = require('./folder1');  // same as require('./folder1/index');

const { function3, function1a } = require('./folder1');

const { functionFolder1 } = require('./folder1/file1');
const function2 = require('./folder1/file2');
// --------------------------------------------

// --- Call functions ------
unknown.function2();
unknown.function3();
unknown.functionFolder1();
unknown.function1a();

function1a();
function3();

functionFolder1();
function2();
// -------------------------