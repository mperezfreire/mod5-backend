const create = require('./create');
const updateUser = require('./update');
const remove = require('./delete');
const list = require('./list');

module.exports = {
  create,
  updateUser,
  remove,
  list,
};
