//Lista de procesos: lsof
//Procesos con conexiones a internet: lsof -i -P
//Procesos Google con conexiones a internet: lsof -i -P | grep Google
//Procesos con nombre node: lsof -c node

console.log(process.pid);

setTimeout(() => {
  console.log('Hello from setTimeout!');
}, 1000);

const interval = setInterval(() => {
  console.log('Hello from setInterval!');
}, 2000);

const timeout = setTimeout(() => {
  console.log('Another timeout!!');
}, 3000);

