function getHora() {
  const hoy=new Date();
  const h=hoy.getHours();
  const m=hoy.getMinutes();
  const s=hoy.getSeconds();
  
  return h+":"+m+":"+s;
}

function imprimirHora(hora){
  console.log(hora);
}

function reloj() {
  const hora = getHora();
  console.log(hora);
}

const interval = setInterval(() => reloj(), 1000);

