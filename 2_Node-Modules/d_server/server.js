const http = require('http');

const hostname = '127.0.0.1';
const port = 8081;

//function myServer(req, res)

const server = http.createServer((req, res) => {
  console.log('We have a request');
  console.log(req.url);
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
