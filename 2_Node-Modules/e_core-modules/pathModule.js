const path = require('path');

console.log(`normalize: ${path.normalize('///home/folderA/../folderA')}`);
console.log(`join: ${path.join('/home/folderA/', 'my-folder')}`);
console.log(`resolve: ${path.join('/home/folderA', './my-folder')}`);
console.log(`dirname: ${path.dirname('/home/folderA/myFile.pdf')}`);
console.log(`extname: ${path.extname('/home/folderA/myFile.pdf')}`);